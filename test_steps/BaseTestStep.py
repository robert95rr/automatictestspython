from abc import ABC, abstractmethod

class BaseTestStep(ABC):
    def __init__(self):
        self.driver = None

    @abstractmethod
    def execute_step(self):
        pass

    @abstractmethod
    def verify_step(self):
        pass

    def set_driver(self, driver):
        self.driver = driver
