from BaseTestStep import BaseTestStep

class GoUnderUrlStep(BaseTestStep):
    def __init__(self, url):
        super().__init__()
        self.url = url

    def execute_step(self):
        self.driver.get(self.url)

    def verify_step(self):
        assert self.url in self.driver.current_url, "Browser is not opened with correct url"

if __name__ == "__main__":
    from selenium import webdriver

    driver = webdriver.Chrome(r"C:\Users\rorozl\.atom\FirstSeleniumTest\Drivers\chromedriver.exe")

    first_step = GoUnderUrlStep("https://docs.python.org/")
    first_step.set_driver(driver)
    first_step.execute_step()
    first_step.verify_step()

    driver.close()
