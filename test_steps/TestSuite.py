from BaseTestStep import BaseTestStep
import unittest

class TestSuite():
    def __init__(self, name = "", test_cases = []):
        super().__init__()
        self.name = name
        for tc in test_cases:
            self.addTest(tc)

    def execute(self):
        for tc in self.test_cases:
            tc.main()

if __name__ == "__main__":
    from GoUnderUrlStep import GoUnderUrlStep
    from SearchByWordDocsStep import SearchByWordDocsStep
    from TestCase import TestCase

    go_under_url_step = GoUnderUrlStep(url = "https://docs.python.org/")
    search_step = SearchByWordDocsStep(string_to_search = "regex", expected_number_of_results = 19)
    search_step_other = SearchByWordDocsStep(string_to_search = "Class", expected_number_of_results = 19, expected_search_time = 60)

    test_case_first = TestCase(name = "Search regex on python doc", preconditions = [], test_steps = [go_under_url_step, search_step])
    test_case_second = TestCase(name = "Search regex on python doc", preconditions = [], test_steps = [go_under_url_step, search_step_other])

    test_suite = unittest.TestSuite()
    test_suite.addTest(test_case_first)
    test_suite.addTest(test_case_second)

    runner = unittest.TextTestRunner()
    runner.run(test_suite)
