from BaseTestStep import BaseTestStep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium import webdriver

class SearchByWordDocsStep(BaseTestStep):
    def __init__(self, string_to_search, expected_number_of_results, expected_search_time = 15):
        super().__init__()
        self.string_to_search = string_to_search
        self.expected_number_of_results = expected_number_of_results
        self.expected_search_time = expected_search_time

    def execute_step(self):
        search_field = self.driver.find_element_by_name("q")
        search_field.send_keys(self.string_to_search)
        search_field.send_keys(Keys.ENTER)

    def verify_step(self):
        wait = WebDriverWait(self.driver, self.expected_search_time)

        is_text_found = True

        try:
            wait.until(EC.text_to_be_present_in_element((By.TAG_NAME, 'h2'), 'Search Results'))
        except TimeoutException:
            is_text_found = False
        assert is_text_found, "Text Search Results is not present"

        listSearch = self.driver.find_element_by_class_name('search')
        items = listSearch.find_elements_by_tag_name("li")

        assert len(items) == self.expected_number_of_results, "Number of search results is not " + str(self.expected_number_of_results)


if __name__ == "__main__":
    first_step = SearchByWordDocsStep("db")
    first_step.execute_step()
    first_step.verify_step()
