from BaseTestStep import BaseTestStep
from selenium import webdriver
import unittest

class TestCase(unittest.TestCase):
    def __init__(self, name, preconditions, test_steps):
        super().__init__()
        self.name = name
        self.preconditions = preconditions
        self.test_steps = test_steps

    def id(self):
        return self.name

    def setUp(self):
        self.driver = webdriver.Chrome(r"C:\Users\rorozl\.atom\FirstSeleniumTest\Drivers\chromedriver.exe")

        for pre in self.preconditions:
            pre.set_driver(self.driver)
        for ts in self.test_steps:
            ts.set_driver(self.driver)

    def runTest(self):
        for pre in self.preconditions:
            pre.execute_step()
            pre.verify_step()
        for ts in self.test_steps:
            ts.execute_step()
            ts.verify_step()

    def tearDown(self):
        self.driver.close()
